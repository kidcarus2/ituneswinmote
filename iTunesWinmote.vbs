Set objShell = WScript.CreateObject("Wscript.Shell")
Set iTunesApp = WScript.CreateObject("iTunes.Application")
WScript.ConnectObject iTunesApp, "iTunes_" ' Allow iTunes COM to call functions prefixed with 'iTunes_'

iTunesApp.Windows.Item(1).Minimized = True ' Hide iTunes window (fully hides if minimize to tray is enabled)
iTunesApp.AppCommandMessageProcessingEnabled = False ' Stop loop bug when media keys trigger OnPlayerPlayEvent triggering media keys etc


' iTunes player events toggle Windows pause/play
Sub iTunes_OnPlayerPlayEvent(track)
	iTunesApp.Stop() ' Stop the song and reset track position to avoid songs playing one after the other
	objShell.SendKeys(chr(&hB3)) ' Simulate pause/play media key press
End Sub


' Set windows volume to match iTunes volume
Sub iTunes_OnSoundVolumeChangedEvent(itunesVolume)
	Dim newVol : newVol = itunesVolume * 655.35 ' newVolume ranged between 0-100 while nircmd sys vol ranges between 0-65535
	objShell.Run "lib\nircmd.exe setsysvolume " & CStr(newVol)
End Sub


' Exit VBScript process automatically when iTunes closes
Sub iTunes_OnAboutToPromptUserToQuitEvent()
	Set iTunesApp = Nothing ' Release iTunes COM connection
	WScript.Quit ' Quit VBScript
End Sub


' Check for updates
objShell.Run "lib\UpdateCheck.vbs"



' Keep script alive and waiting for events (WScript.Quit is only responsive between sleeps, don't sleep too long here)
Do While True
    WScript.Sleep 1000
Loop