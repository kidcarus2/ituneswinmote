# Welcome to iTunesWinmote
The PC media remote-control using your iPhone, iPad and Apple Watch!

## Installation
1. Install iTunes
2. Add at least 1 song to your library - if you don't have any drag and drop iTunesWinmote.wav into your library!
3. Launch iTunesWinmote.vbs

## How to use
1. Install the iTunes remote app on your device (pre-installed for Apple Watch)
2. Use the pause/play button to trigger Windows pause/play
3. Change the volume to control your PC's volume

## Running on startup
To run on startup, place a shortcut to iTunesWinmote.vbs inside the folder `%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup`

## Notes
* I suggest installing [StreamKeys](https://www.streamkeys.com/) for Google Chrome, enabling web music and video control!
* Set iTunes to show system tray icon and minimize to tray under advanced preferences to auto-hide the iTunes window when loading this script!
* iTunes does not need to be open before running iTunesWinmote.vbs
* Want to develop this further? Head over to the Apple Developer downloads page and checkout the iTunesCOM SDK
* Feature request or feedback? Click [here](https://www.reddit.com/r/AppleWatch/comments/9ymf6m/release_ituneswinmote_windows_pc_media_remote/) for the Reddit release post!