Dim CurrentDir : CurrentDir = replace(WScript.ScriptFullName, WScript.ScriptName, "")
Dim SettingsFile : SettingsFile = CurrentDir & "UpdateCheckSettings.xml"


' Create XML reader object
Set xmlSettingsReader = CreateObject("MSXML2.DOMDocument")

' Load UpdateCheck settings from XML file
Dim updateCheckXML : updateCheckXML = CreateObject("Scripting.FileSystemObject").OpenTextFile(SettingsFile, 1).ReadAll
xmlSettingsReader.loadXML(updateCheckXML)

' Check for XML errors
If xmlSettingsReader.parseError <> 0 Then
	WScript.Echo xmlSettingsReader.parseError.reason
	WScript.Quit 1
End If

Dim appName : appName = xmlSettingsReader.SelectNodes("settings/appName")(0).Text
Dim downloadPage : downloadPage = xmlSettingsReader.SelectNodes("settings/downloadPage")(0).Text
Dim rssFeedURL : rssFeedURL = xmlSettingsReader.SelectNodes("settings/rssFeedURL")(0).Text
Dim currentVersionCommitID : currentVersionCommitID = xmlSettingsReader.SelectNodes("settings/currentVersionCommitID")(0).Text


' Download feed
Set httpObject = CreateObject("MSXML2.XMLHTTP")
httpObject.open "GET", rssFeedURL, False
httpObject.send


' Load feed into xmlFeedReader
Set xmlFeedReader = CreateObject("MSXML2.DOMDocument")
xmlFeedReader.loadXML(httpObject.responseText)

' Check for XML errors (maybe the feed couldn't download?)
If xmlFeedReader.parseError <> 0 Then
	WScript.Echo xmlFeedReader.parseError.reason
	WScript.Quit 1
End If


Set lastCommit = xmlFeedReader.SelectNodes("//entry")(0)
Dim lastCommitID : lastCommitID = lastCommit.SelectNodes(".//id")(0).Text

' Get commit reason
Dim lastCommitSummary : lastCommitSummary = lastCommit.SelectNodes(".//summary")(0).Text
Dim lastCommitTitle : lastCommitTitle = lastCommit.SelectNodes(".//title")(0).Text
Dim lastCommitReason : lastCommitReason = lastCommitSummary
If lastCommitSummary = "" Then
	lastCommitReason = lastCommitTitle
End If


If currentVersionCommitID = lastCommitID Then
	'MsgBox appName & " is up to date!", vbOKOnly, ""
Else
	result = MsgBox ("Changelog: " & vbCrLf & lastCommitReason & vbCrLf & vbCrLf & "Would you like to visit the download page?", vbYesNo, appName & " update available!")
	
	If result = vbYes Then
		WScript.CreateObject("WScript.Shell").Run(downloadPage)
		
		xmlSettingsReader.SelectNodes("settings/currentVersionCommitID")(0).Text = lastCommitID
		xmlSettingsReader.Save SettingsFile
	End If
End If